let getUsuarioById = (id, callback) => {
    let usuario = {
        nombre: 'Fernando',
        id
    }

    if( id === 20){
        callback(`usuario con id ${ id }, no existe en la base de datos`)
    } else {
        callback(null, usuario)
    }

} 


getUsuarioById(10, (err, usuario) => {

    if( err ) {
        return console.log(err)
    }
    console.log("usuario de base de datos", usuario)
})