let empleados = [{
    id: 1,
    nombre: 'Leandro'
}, {
    id: 2,
    nombre: 'Pepe'
}, {
    id: 3,
    nombre: 'Roberto'
}]


let salarios = [{
    id: 1,
    salario: 1000
}, {
    id: 2,
    salario: 850
}]

let getEmpleado = async(id) => {
        let empleadoDB = empleados.find( empleado => empleado.id === id)

        if( !empleadoDB ){
            throw new Error(`No existe en la BD el empleado con el ID ${ id }`)
        }else {
            return empleadoDB
        }   
}

let getSalario = async(empleado) => {

        let salarioDB = salarios.find( salario => salario.id === empleado.id)

        if( !salarioDB ){
            throw new Error(`No se encontro un salario para el usuario ${ empleado.nombre }`)
        } else {
            return {
                nombre: empleado.nombre,
                salario: salarioDB.salario,
                id: empleado.id
            }
        }

}

let getInformacion = async(id) => {

    let empleado = await getEmpleado(id)
    let salario = await getSalario(empleado)

    return `${salario.nombre} tiene un salario de $ ${salario.salario}`


}

getInformacion(5).then(mensaje => {
    console.log(mensaje)
}).catch(err => {
    console.log(err)
});