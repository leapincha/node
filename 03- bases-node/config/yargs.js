const options = {
    base: {
        demand: true,
        alias: 'b'
    }, 
    limite: {
        alias: 'l',
        default: 10
    }
}

const argv = require('yargs')
                .command('listar', 'Imprime en consola la tabla multiplicar', options).argv;


module.exports = {
    argv
}