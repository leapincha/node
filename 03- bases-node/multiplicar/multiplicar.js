

const fs = require('fs');

let listarTabla = (base, limite = 10) => {
    for (let i = 1; i <= limite; i++) {
        console.log(`${base} * ${i} = ${base * i}`)
    }
}

let crearArchivo = (base) => {
    return new Promise ( (resolve, reject) => {

        if( !Number(base) ) {
            reject('No es un numero')
            return;
        }
        let data = '';

        for (let i = 1; i <= 10; i++) {
            let resultado = i*base
            data += `${base} * ${i} = ${resultado} \n`;
        }
        
        fs.writeFile(`tablas/tabla-${base}.txt`, data, (err) => {
          if (err) reject(err);
          resolve(`tabla-${base}.txt`);
        });
    })
}

module.exports = {
    crearArchivo,
    listarTabla
}

