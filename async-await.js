const { errorMonitor } = require("stream")

/* let getNombre = async() => {
    throw new Error('No existe un nombre para ese usuario')
    undefined.nombre
    return 'Fernando'
} */

let getNombre = async() => {
    return new Promise( (resolve, reject) => {
        setTimeout(() => {
            resolve('Fernando') 
        }, 3000)
    })
}

let saludo = async() => {

    let nombre = await getNombre();
    return `Hola ${nombre}`
}

saludo().then(mensaje => {
    console.log(mensaje) 
}).catch(err => {
    console.log(err)
})